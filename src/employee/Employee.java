/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Employee {


	private String employeeId;
	private String name;
	private Date dateofJoining;

	public Employee(String employeeId, String name, Date dateofJoining)
	{
		this.employeeId = employeeId;
		this.name = name;
		this.dateofJoining = dateofJoining;
	}

    Employee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

public String getEmployeeId()
{
	return this.employeeId;
}

public String getEmployeeName()
{
	return this.name;
}

public Date getEmployeeDateofJoining()
{
	return this.dateofJoining;
}

public String toString()
{
	return "Employee Id:" + getEmployeeId() + "\nEmployee Name:" + getEmployeeName() +"\nJoin Date" + getEmployeeDateofJoining();
}
}
